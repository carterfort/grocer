<?php

namespace spec\Grocer\Converters;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ConvertsStringToIngredientSpec extends ObjectBehavior
{

    function it_converts_ingredients_by_themselves()
    {
        $this->convert('cilantro')
        ->shouldReturn([
            'amount' => 1.0,
            'measurement' => null,
            'name' => 'cilantro'
        ]);
    }


    function it_converts_ingredients_with_spaces_in_the_name_by_themselves()
    {
        $this->convert('mozzerella cheese')
        ->shouldReturn([
            'amount' => 1.0,
            'measurement' => null,
            'name' => 'mozzerella cheese'
        ]);
    }

    function it_converts_ingredients_with_color_modifiers()
    {
        $this->convert('1 red onion')
        ->shouldReturn([
            'amount' => 1.0,
            'measurement' => null,
            'name' => 'red onion'
        ]);
    }

    public function it_converts_simple_ingredient()
    {
    	$this->convert('1 lemon')->shouldReturn([
            'amount' => (float) 1,
            'measurement' => null,
            'measurement' => null,
            'name' => 'lemon'
        ]);
    }

    function it_converts_multiples_of_the_same_ingredient()
    {
        $this->convert('18 lemons')->shouldReturn([
            'amount' => (float) 18,
            'measurement' => null,
            'measurement' => null,
            'name' => 'lemons'
        ]);
    }

    public function it_converts_a_can_of_tomato_sauce()
    {


    	$this->convert('1 can tomato sauce')
    	->shouldReturn([
            'amount' => (float) 1,
            'measurement' => 'can',
            'name' => 'tomato sauce',
    	]);
    }	

    function it_converts_fractions_of_amounts()
    {
        $this->convert('1/4 cup honey')
        ->shouldReturn([
            'amount' => (float) 0.25,
            'measurement' => 'cup',
            'name' => 'honey'
        ]);
    }

    function it_converts_whole_numbers_plus_fractions()
    {
        $this->convert('5 3/4 cup honey')
        ->shouldReturn([
            'amount' => (float) 5.75,
            'measurement' => 'cup',
            'name' => 'honey'
        ]);
    }

    function it_handles_thinly_sliced_preparation_instructions()
    {
        $this->convert('1/2 cup thinly sliced red onions')
        ->shouldReturn([
            'amount' => (float) 0.5,
            'measurement' => 'cup',
            'name' => 'red onions',
            'preparation' => 'thinly sliced'
        ]);
    }

    function it_handles_nonsensical_trademarks()
    {
        $this->convert('1 1/2 teaspoons Mazola® Corn Oil')
        ->shouldReturn([
            'amount' => (float) 1.5,
            'measurement' => 'teaspoons',
            'name' => 'Mazola® Corn Oil',
        ]);
    }

    function it_converts_package_sizes()
    {
        $this->convert('1 (5 ounce) package fresh baby spinach')
        ->shouldReturn([
                'amount' => (float) 1,
                'measurement' => 'package',
                'name' => 'fresh baby spinach',
                'weight' => '(5 ounce)'
            ]);
    }

    function it_converts_multiple_package_sizes()
    {
        $this->convert('3 (5 ounce) package fresh baby spinach')
        ->shouldReturn([
                'amount' => (float) 3,
                'measurement' => 'package',
                'name' => 'fresh baby spinach',
                'weight' => '(5 ounce)'
            ]);
    }

    function it_provides_a_failsafe_catchall_for_silly_nonsense_items()
    {

        $this->convert('Freshly ground Spice Islands® Black Pepper Adjustable Grinder')
        ->shouldReturn([
                'amount' => 1.0,
                'measurement' => null,
                'name' => 'Freshly ground Spice Islands® Black Pepper Adjustable Grinder'
            ]);
    }

    function it_handles_chopped_and_toasted_preparation_instructions()
    {
        $this->convert('1 tablespoon chopped, toasted pine nuts')
        ->shouldReturn([
            'amount' => (float) 1,
            'measurement' => 'tablespoon',
            'name' => 'pine nuts',
            'preparation' => 'chopped, toasted'
        ]);
    }

    function it_handles_preparation_instructions_with_package_sizes()
    {
        $this->convert('1 (5 ounce) package chopped, thinly sliced fresh baby spinach')
        ->shouldReturn([
                'amount' => (float) 1,
                'measurement' => 'package',
                'name' => 'fresh baby spinach',
                'preparation' => 'chopped, thinly sliced',
                'weight' => '(5 ounce)',
            ]);
    }

    function it_handles_chopped_cloves_of_garlic()
    {
        $this->convert('4 cloves garlic, chopped')
        ->shouldReturn([
            'amount' => 4.0,
            'measurement' => 'cloves',
            'name' => 'garlic'
        ]);
    }
}
