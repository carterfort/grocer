@extends('layouts.standard')

@section('body')
	
	<div id="list">
	<h2>Shopping List for {{$weeklyList->starts_at->format('m-d-Y')}}</h2>
	<h5>Items remaining: <span v-text="items.length"></span> </h5>

	<div class="list-group">

		<div class="list-group-item" v-repeat="item in items" v-on="click: item.isInStock = !item.isInStock ">
			<input type="checkbox" v-model="item.isInStock">
				<span v-class="
					strikethrough: item.isInStock
					">
				@{{ item.singleLineDescription }}
				</span>
		</div>

	</div>

	</div>

	<pre>
		{{$weeklyList->ingredients}}
	</pre>


	<style>
		#list .list-group-item {
			-webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}

		.strikethrough {
			text-decoration: line-through;
		}
	</style>
	<script>
		new Vue({
			el : '#list',
			data : {
				items :	{!!$weeklyList->ingredients!!}
			}
		});
	</script>

@stop