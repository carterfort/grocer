<?php

namespace Grocer\Models;

use Grocer\ItemInStock;
use Illuminate\Database\Eloquent\Model;
use Grocer\Converters\IngredientListConverter;
use Grocer\Converters\ConvertsStringToIngredient;

class Ingredient extends Model
{
    //
    //
    
    public function toArray() {
        $array = parent::toArray();
        $array['singleLineDescription'] = $this->singleLineDescription;
        return $array;
    }

    protected $fillable = [
        'name',
        'store_section'
    ];

    public static function createForRecipeWithIngredientsString($recipe, $ingredients)
    {

        $converter = new IngredientListConverter(new ConvertsStringToIngredient);
        $items = $converter->convertedList($ingredients);

        foreach ($items as $item)
        {
            if (isset($item['catch-all'])) continue;
            
            $ingredient = static::firstOrCreate(['name' => $item['name']]);

            $recipe->ingredients()->attach($ingredient->id, [
                    'amount' => $item['amount'],
                    'measurement' => $item['measurement']
                ]);
        }
    }

    public function inStock()
    {
        return $this->hasMany(ItemInStock::class);
    }

    public function recipes()
    {
        return $this->belongsToMany(Recipe::class);
    }

    public function getSingleLineDescriptionAttribute()
    {
        $string = [];
        $string[] = $this->pivot->amount;
        $string[] = $this->pivot->measurement;
        $string[] = $this->name;

        return implode(' ', $string);
    }

    public function __toString()
    {
        
        $string[] = $this->pivot->amount;
        $string[] = $this->pivot->measurement;
        $string[] = $this->name;

        return implode(' ', $string);

    }

    public function isInStockForWeeklyListId($weeklyListId)
    {
        return $this->inStock()->forWeeklyListId($weeklyListId)->count();
    }
}
