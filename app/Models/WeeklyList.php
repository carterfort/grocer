<?php

namespace Grocer\Models;

use Carbon\Carbon;
use Grocer\Models\Recipe;
use Grocer\Models\Ingredient;
use Grocer\AssemblesIngredientList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class WeeklyList extends Model
{
    //

	protected $fillable = [
		'starts_at'
	];

	protected $dates = [
		'starts_at'
	];



	public static function currentWeek()
	{
		$startDate = Carbon::now()->startOfWeek()->subDays(1);
		return static::firstOrCreate(['starts_at' => $startDate]);
	}

    public function recipes()
    {
        return $this->belongsToMany(Recipe::class)->withPivot(['meal_time', 'meal_date']);
    }

    public function ingredients()
    {
    	
        $assembler = new AssemblesIngredientList;
        $assembler->listId = $this->id;
        return $assembler->forRecipes($this->recipes);
        
    }

    public function getIngredientsAttribute()
    {
    	return $this->ingredients();
    }
}
