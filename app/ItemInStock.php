<?php

namespace Grocer;

use Illuminate\Database\Eloquent\Model;

class ItemInStock extends Model
{

	protected $fillable = [
		'ingredient_id',
		'weekly_list_id'
	];

	public $table = 'items_in_stock';

	public function scopeForWeeklyListId($query, $weeklyListId)
	{
		$query->whereWeeklyListId($weeklyListId);
	}

    public static function toggleIngredientWithIdForList($ingredientId, $listId)
    {
    	$item = static::whereIngredientId($ingredientId)->whereWeeklyListId($listId);
    	if ($item->count() > 0)
    	{
    		$item->delete();
    	} else {
    		static::create(['ingredient_id' => $ingredientId, 'weekly_list_id' => $listId]);
    	}
    }
}
