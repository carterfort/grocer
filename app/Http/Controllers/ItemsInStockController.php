<?php

namespace Grocer\Http\Controllers;

use Grocer\ItemInStock;
use Grocer\Http\Requests;
use Illuminate\Http\Request;
use Grocer\Http\Controllers\Controller;

class ItemsInStockController extends Controller
{

    protected $itemInStock;

    public function __construct(ItemInStock $itemInStock)
    {
        $this->itemInStock = $itemInStock;
    }
    public function toggleItemInStockStatus($ingredientId, $listId)
    {

        $this->itemInStock->toggleIngredientWithIdForList($ingredientId, $listId);
        return response()->json(['success' => true]);
        
    }
}
