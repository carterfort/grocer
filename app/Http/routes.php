<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PlannerController@planner');

Route::resource('recipes', 'RecipesController');

Route::get('weekly-lists/current-list', ['as' => 'weekly-lists.current-list', 'uses' => 'WeeklyListsController@currentList']);
Route::resource('weekly-lists', 'WeeklyListsController');

Route::get('ingredients/items-in-stock/toggle/{$ingredientId}/{$weeklyListId}', ['as' => 'toggle-item-in-stock-for-ingredient-for-list', 'uses' => 'ItemsInStockController@toggleItemInStockStatus']);