<?php

namespace Grocer;

use Illuminate\Database\Eloquent\Collection;

class AssemblesIngredientList {

    public $listId;
	
	/**
	 * Take a Collection of recipes and return a Collection of ingredients, ordered by name and with correct amounts, grouped by measurement
	 * i.e. If you have two recipes, each calling for 1.25 cups of mozzerrella cheese, this will return a collection with one ingredient named mozzerrella cheese with an amount of 2.5 cups
	 * 
	 * @param  Illuminate\Database\Eloquent\Collection $recipes
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function forRecipes(Collection $recipes)
	{
        $ingredients = new Collection();

    	foreach ($recipes as $recipe)
    	{
            foreach ($recipe->ingredients as $ingredient)
            {
                $existingIngredient = $ingredients->filter(function($collectedIngredient) use ($ingredient){
                        return ($ingredient->name == $collectedIngredient->name) && ($ingredient->pivot->measurement == $collectedIngredient->pivot->measurement);
                    })->first();

                if ($existingIngredient)
                {
                    $existingIngredient->amount += $ingredient->pivot->amount;
                } else {
                    $ingredient->amount = $ingredient->pivot->amount;
                    $ingredient->isInStock = $ingredient->isInStockForWeeklyListId($this->listId);
                    $ingredients->add($ingredient);
                }
            }
    	}

        $ingredients = $ingredients->sortBy('name');

    	return $ingredients;
	}

}