<?php

namespace Grocer\Converters;

class ConvertsStringToIngredient
{
	public function convert($string)
	{
		$measurements = implode('|', $this->measurementWords);
		$measurementPattern = '/(.+) ('.$measurements.') (.+)/';
		$stringMatchesAMeasurementPattern = preg_match($measurementPattern, $string, $measurementData);

		if ( ! $stringMatchesAMeasurementPattern)
		{
			return $this->explodeStringWithSpacesIntoIngredient($string);
		}

		//Not sure what to do with this here. Apparently the extra whoosits is mucking up the works, capiche?

		array_shift($measurementData);

		$stringContainsAWeightMeasurement = preg_match('/(.+) (\(([^\)]+)\))/', $measurementData[0], $amountAndWeight);

		if ($stringContainsAWeightMeasurement)
		{
			return $this->mergeAmountAndWeightWithIngredientDataIntoIngredient($amountAndWeight, $measurementData);
		}
		
		return $this->standardDivisonOfAmountAndNameIntoIngredient($measurementData);

	}

	private function explodeStringWithSpacesIntoIngredient($string)
	{
			$measurementData = explode(' ', $string);

			if (count($measurementData) == 1)
			{
				$ingredient['amount'] = 1.0;
				$ingredient['measurement'] = null;
				$ingredient['name'] = $string;
			} else {

				$ingredient['amount'] = (float) $measurementData[0];
				$ingredient['measurement'] = null;

				array_shift($measurementData);
				$this->handleNameAndPreparationForIngredient($ingredient, implode(' ', $measurementData));

				if ( $ingredient['amount'] == 0.0)
				{
					return [
						'amount' => 1.0,
						'measurement' => null,
						'name' => $string
					];
				}

			}
			return $ingredient;
	}

	private function mergeAmountAndWeightWithIngredientDataIntoIngredient($amountAndWeight, $measurementData)
	{
			$ingredient['amount'] = $this->convertAmountToFloat($amountAndWeight[1]);
			$ingredient['measurement'] = $measurementData[1];
			
			$this->handleNameAndPreparationForIngredient($ingredient, $measurementData[2]);

			$ingredient['weight'] = $amountAndWeight[2];


			return $ingredient;
	}

	private function standardDivisonOfAmountAndNameIntoIngredient($measurementData)
	{

			$ingredient['amount'] = $this->convertAmountToFloat($measurementData[0]);
			$ingredient['measurement'] = $measurementData[1];

			$this->handleNameAndPreparationForIngredient($ingredient, $measurementData[2]);

			return $ingredient;
	}

	private function handleNameAndPreparationForIngredient(&$ingredient, $name)
	{
		$name = trim($name);
		
		$preparationPattern = '/('.implode('|', $this->preparations).')/';
		$preparationData = preg_split($preparationPattern, $name, -1, PREG_SPLIT_DELIM_CAPTURE);

		if ( $preparationData[0])
		{
			$name = trim(str_replace(',', '', trim($preparationData[0])));
			$ingredient['name'] = $name;
		} else {

			$ingredient['name'] = str_replace(',', '', trim($preparationData[ count($preparationData) - 1]));
			array_pop($preparationData);

			$scrubbedPreparations = [];
			foreach ($preparationData as $prepPoint)
			{
				if ($prepPoint != '' && $prepPoint != ', ') $scrubbedPreparations[] = $prepPoint;
			}

			$ingredient['preparation'] = trim(implode(', ', $scrubbedPreparations ));
		}
	}

	private function convertAmountToFloat($amount)
	{
			$remainder = 0;

			if (str_contains($amount, '/')){
				$divisionmeasurementData = explode('/', $amount);

				$divisee = $divisionmeasurementData[0];
				$divisor = $divisionmeasurementData[1];


				if (str_contains($divisionmeasurementData[0], ' '))
				{
					$splitter = explode(' ', $divisee);
					$remainder = $splitter[0];
					$divisee = $splitter[1];
				}

				$amount =  $divisee / $divisor;
			}

			return (float) $amount + $remainder;
	}


	private $measurementWords = [
		'cup',
		'cups',
		'can',
		'cans',
		'teaspoon',
		'teaspoons',
		'tablespoon',
		'tablespoons',
		'package',
		'packages',
		'packet',
		'packets',
		'envelope',
		'envelopes',
		'box',
		'boxes',
		'pinch',
		'pinches',
		'dash',
		'dashes',
		'pound',
		'pounds',
		'lb',
		'lbs',
		'oz',
		'ounces',
		'clove',
		'cloves',
		'clove of',
		'cloves of',
	];

	private $preparations = [
		'thinly sliced',
		'sliced',
		'chopped',
		'toasted',
		'finely chopped',
		'peeled and deveined',
		'juiced',
		'pitted and diced',
	];
}
