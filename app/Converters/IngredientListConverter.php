<?php 

namespace Grocer\Converters;

class IngredientListConverter {

	protected $converter;

	public function __construct($converter)
	{
		$this->converter = new ConvertsStringToIngredient;
	}

	public function convertedList($ingredientsString)
	{
		$separatedItems = $this->separateItems($ingredientsString);

		$items = [];

		foreach ($separatedItems as $itemString)
		{
			$items[] = $this->converter->convert($itemString);
		}

		return $items;
	}


	private function separateItems($itemsText)
	{
		return array_filter(preg_split("/\n|\r/", $itemsText));
	}

	

}